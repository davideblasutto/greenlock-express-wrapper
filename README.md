# greenlock-express-wrapper

This is just a no-nonsense wrapper for [greenlock-express](https://git.daplie.com/Daplie/greenlock-express).

## Usage

### Example
```javascript
var server = require("express")()
// Obviously here you have to define Express behavior...
require("greenlock-express-wrapper").listen({
	express: server,
	email: "admin@domain.com",
	approveDomains: [ "domain.com", "sub.domain.com" ],
	plainPort: 8080,
	staging: false,
	verbose: true,
	logFunction: function(message) { console.log(message) }
})
// Now Express is listening on port 443 with SSL and on plain port 8080!
```

### Options
 * `express` - The Express server object.
 * `email` - The default email to use to accept agreements.
 * `approveDomains` - An array of allowed domains.
 * `plainPort` - *Optional*. If set, Express server will also listen via HTTP on that port.
 * `staging` - *Optional*. If true, greenlock-express will use the *staging* Let's Encrypt server.
 * `verbose` - *Optional*. If true, greenlock-express will log (see below) the server bootstrap.
 * `logFunction` - *Optional*. Custom function for logging, with signature `logFunction(message)`. Default is `console.log`.

## Dependencies
Note: the package `express` isn't in the dependencies list. You'll have to install it in the main package.
