# 1.1.3 (20/03/2018)

* Code sanitized

# 1.1.0 (06/11/2017)

* Better logging: added `verbose` and `logFunction` parameters

# 1.0.7 (12/10/2017)

* Added `staging` parameter

# 1.0.0 (09/10/2017)

First release
